<?php 

    class App{

        private $servidor;
        private $usuario;
        private $clave; 
        private $base;
        private $conexion; 

        public function __construct(){
            $this -> servidor = "localhost";
            $this -> usuario = "root";
            $this -> clave = "";
            $this -> base = "alki";
            $this -> conexion = new mysqli($this -> servidor,$this -> usuario,$this -> clave, $this -> base);
            $this -> conexion -> set_charset("utf-8");
        }

        public function consultaMysql($sql){
            $respuesta = [];
            $respuesta = $this -> conexion -> query($sql);
            return $respuesta -> fetch_all();
        }

        public function ejecutaMysql($sql){
            return $this -> conexion -> query($sql);
        }

        public function cerrar(){
            $this -> conexion -> close();
        }

        public function __destruct(){}

    }
