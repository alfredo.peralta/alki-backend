<?php 

    class Token{

        public static function generaToken($datos){
            /** Se declara tipo cifrado y passoword **/
            $password = "ALKI-208081.aer";
            $metodo = "AES-256-CBC";
            /** Se crea token **/
            $ivSize = openssl_cipher_iv_length($metodo);
            $iv = openssl_random_pseudo_bytes($ivSize);
            $datosCifrados = openssl_encrypt($datos, $metodo, $password, 1, $iv);
            return base64_encode($iv . $datosCifrados);
        }

        public static function descifrarToken($token){
            /** Se declara tipo cifrado y passoword **/
            $password = "ALKI-208081.aer";
            $metodo = "AES-256-CBC";
            /** Se decencripta token **/
            $datos = base64_decode($token);
            $ivSize = openssl_cipher_iv_length($metodo);
            
            $iv = substr($datos, 0, $ivSize);
            $datosCifrados = substr($datos, $ivSize);

            return json_decode(openssl_decrypt($datosCifrados, $metodo, $password, 1,$iv));
        }

        public static function verificaToken($token){
            /** Se declara tipo cifrado y passoword **/
            $password = "ALKI-208081.aer";
            $metodo = "AES-256-CBC";
            /** Se decencripta token **/
            $datos = base64_decode($token);
            $ivSize = openssl_cipher_iv_length($metodo);
            $respuesta = false;
            
            $iv = substr($datos, 0, $ivSize);
            $datosCifrados = substr($datos, $ivSize);

            $descifrado = json_decode(openssl_decrypt($datosCifrados, $metodo, $password, 1,$iv));

            if(isMobileDevice()){
                $respuesta = true;
            }else{
                if(strtotime($descifrado[0]["live"]) <= strtotime(date("Y-m-d H:i:s"))){
                    $respuesta = true;
                }
            }
            return $respuesta;
        }

        function isMobileDevice() {
            return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo
        |fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i"
        , $_SERVER["HTTP_USER_AGENT"]);
        }

    }