<?php 

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

    /** Se llaman dao y modelo seguridad **/
    require_once '../dao/SeguridadDAO.php';
    require_once '../modelo/SeguridadModelo.php';
    require_once '../config/Token.php';

    /** Se declaran variables **/
    $token = $_POST["token"];
    $id = $_POST["id"];
    $estatus = $_POST["estatus"];

    $datos = Token::descifrarToken($token);
    $respuesta = null;
    $code = null;

    if($datos -> tipo != 1){
        SeguridadDAO::editaEstatusAlertaSeguridad($estatus,$id);
        $respuesta = "Estatus Actualizado!";
        $code = "success";
    }else{
        $respuesta = "ERROR AL ACTUALIZAR";
        $code = "error";
    }

    echo json_encode(array('response'=>$respuesta, 'code'=>$code));