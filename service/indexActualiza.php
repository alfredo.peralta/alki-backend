<?php 
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

    /** Se llaman dao y modelo seguridad **/
    
    require_once '../dao/SeguridadDAO.php';
    require_once '../modelo/SeguridadModelo.php';
    require_once '../dao/ResidenteDAO.php';
    require_once '../modelo/ResidenteModelo.php';
    require_once '../config/Token.php';

    /** Se declaran variables **/
    $token = $_POST["token"];
    $nombre = $_POST["nombre"];
    $apellidos = $_POST["apellidos"];
    $correo = $_POST["correo"];
    $telefono = $_POST["telefono"];
    $password = $_POST["password"];
    
    $datos = Token::descifrarToken($token);
    $respuesta = null;
    $contt = 0;

    $residente = new Residente();
    $residente -> correo = $datos -> correo;
    $respuesta = ResidenteDAO::consultaResidente($residente);
    if($respuesta != null){
        $residente -> id_residente = $respuesta[0][2];
        $contt = $contt + 1;
    }
    
    $seguridad = new Seguridad();
    $seguridad -> correo = $datos -> correo;
    $respuesta = SeguridadDAO::consultaSeguridad($seguridad);
    if($respuesta != null){
        $seguridad -> id_seguridad = $respuesta[0][2];
        $contt = $contt + 1;
    }

    if($datos -> tipo == 1 && $contt == 1){
        $residente -> nombre = $nombre;
        $residente -> apellidos = $apellidos;
        $residente -> correo = $correo;
        $residente -> password = $password;
        $residente -> telefono = $telefono;
        $respuesta = guardarDatosResidente($residente);
    }else if($datos -> tipo != 1 && $contt == 1){
        $seguridad -> nombre = $nombre;
        $seguridad -> apellidos = $apellidos;
        $seguridad -> correo = $correo;
        $seguridad -> password = $password;
        $seguridad -> telefono = $telefono;
        $respuesta = guardarDatosSeguridad($seguridad);
    }else{
        $respuesta = "Error al guardar!";
    }

    function guardarDatosResidente($residente){
        $response = null;
        $valid = 0;

        if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $residente -> correo)){
            $valid = $valid + 1;
        }else{
            $response = "Su correo eléctronico no es valido";
        }

        if(preg_match('/^[0-9a-zA-Z!#$%*@]+$/',$residente -> password)){
            $valid = $valid + 1;
        }else{
            $response = "Su contraseña no es valida.";
        }

        if(preg_match('/^[0-9]+$/',$residente -> telefono)){
            $valid = $valid + 1;
        }else{
            $response = "Su teléfono no es valido.";
        }

        if(preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/',$residente -> nombre)){
            $valid = $valid + 1;
        }else{
            $response = "Su nombre no es valido.";
        }

        if(preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/',$residente -> apellidos)){
            $valid = $valid + 1;
        }else{
            $response = "Sus apellidos no son validos. ";
        }

        if($valid == 5){
            ResidenteDAO::editaIndexResidente($residente);
            $response = "Datos Guardados!";
        }

        return $response;
    }

    function guardarDatosSeguridad($seguridad){
        $response = null;
        $valid = 0;

        if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $seguridad -> correo)){
            $valid = $valid + 1;
        }else{
            $response = "Su correo eléctronico no es valido";
        }

        if(preg_match('/^[0-9a-zA-Z!#$%*@]+$/',$seguridad -> password)){
            $valid = $valid + 1;
        }else{
            $response = "Su contraseña no es valida.";
        }

        if(preg_match('/^[0-9]+$/',$seguridad -> telefono)){
            $valid = $valid + 1;
        }else{
            $response = "Su teléfono no es valido.";
        }

        if(preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/',$seguridad -> nombre)){
            $valid = $valid + 1;
        }else{
            $response = "Su nombre no es valido.";
        }

        if(preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/',$seguridad -> apellidos)){
            $valid = $valid + 1;
        }else{
            $response = "Sus apellidos no son validos. ";
        }

        if($valid == 5){
            SeguridadDAO::editaIndexSeguridad($seguridad);
            $response = "Datos Guardados!";
        }

        return $response;
    }


    $jsonAnswer = array('respuesta' => $respuesta);
    echo json_encode($jsonAnswer);