<?php 
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

    /** Se llaman dao y modelo seguridad **/
    require_once '../dao/ResidenteDAO.php';
    require_once '../modelo/ResidenteModelo.php';
    require_once '../config/Token.php';

    /** Se declaran variables **/
    $token = $_POST["token"];

    $datos = Token::descifrarToken($token);
    $respuesta = null;
    $code = null;

    if($datos -> tipo == 1){
        $residente = new Residente();
        $residente -> correo = $datos -> correo;
        $respuesta = ResidenteDAO::insertaAlertaResidente($datos -> id);
        $code = "success";
        $respuesta = "La alerta fue enviada!";
    }else{
        $code = "error";
        $respuesta = "No se logro solicitar la alerta.";
    }

    echo json_encode(array('code' => $code, 'response' => $respuesta));