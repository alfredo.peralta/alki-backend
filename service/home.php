<?php 
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

    /** Se llaman dao y modelo seguridad **/
    require_once '../config/Token.php';

    /** Se declaran variables **/
    $token = $_POST["token"];

    $datos = Token::descifrarToken($token);

    $residente = false;

    if($datos -> tipo == 1){
       $residente = true; 
    }

    $jsonAnswer = array('correo' => $datos -> correo, 'oaut' => $residente, 'id' => $datos -> id, 'live' => $datos -> live);
    echo json_encode($jsonAnswer);