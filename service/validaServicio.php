<?php 
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

    require_once '../dao/ResidenteDAO.php';
    require_once '../modelo/ResidenteModelo.php';
    require_once '../config/Token.php';

    /** Se declaran variables **/
    $token = $_POST["token"];
    $datos = Token::descifrarToken($token);
    $response = null;
    $code = null;

    /** consulta servicio **/
    $respuesta = ResidenteDAO::consultaEstatusResidentePorId($datos -> id);

    if($respuesta[0][0] != "Bloqueado"){
        $code = "sucess";
        $response = "Servicio activo";
    }

    if($respuesta[0][0] != "Suspendido"){
        $code = "sucess";
        $response = "Servicio activo";
    }

    if($respuesta[0][0] == "Suspendido"){
        $code = "info";
        $response = "Su servicio ha vencido!";
    }

    $jsonAnswer = array("code" => $code, "response" => $response);
    echo json_encode($jsonAnswer);