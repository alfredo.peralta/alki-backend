<?php 
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

    /** Se llaman dao y modelo seguridad **/
    require_once '../dao/SeguridadDAO.php';
    require_once '../modelo/SeguridadModelo.php';
    require_once '../dao/ResidenteDAO.php';
    require_once '../modelo/ResidenteModelo.php';
    require_once '../config/Token.php';

    /** Se declaran variables **/
    $token = $_POST["token"];

    $datos = Token::descifrarToken($token);
    $respuesta = null;

    if($datos -> tipo == 1){
        $respuesta = ResidenteDAO::consultaVisitasResidente($datos -> id);
    }

    $data = array();
    
    $datos = array('data' => $respuesta);

    if(count($datos["data"]) > 0){
        for($i = 0; $i < count($datos["data"]); $i++){
            $response = ['fecha' => $datos["data"][$i][0], 'descripcion' => $datos["data"][$i][1]];
            array_push($data, $response);
        }
    }

    echo json_encode($data);