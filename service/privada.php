<?php 
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

    /** Se llaman dao y modelo seguridad **/
    require_once '../dao/PrivadaDAO.php';
    require_once '../modelo/PrivadaModelo.php';
    require_once '../config/Token.php';
    $method = isset($_GET['method']) ? $_GET['method'] : '';

switch ($method) {
    case 'listarPrivadas':
        listarPrivadas();
        break;
    // Puedes agregar más casos aquí para otros métodos
    default:
        echo json_encode(["message" => "Método no permitido"]);
        http_response_code(405); // Método no permitido
        break;
}


    /** Se declaran variables **/
    $token = null;

    $mensaje = null;
    

    function listarPrivadas(){
        try {
    
    
            // Llamar al método de DAO para listar privadas
            $estatus = PrivadaDAO::listarPrivadas();
    
            // Verificar si hay resultados
            if ($estatus === false || empty($estatus)) {
                throw new Exception("No se encontraron resultados o hubo un error en la consulta.");
            }
    
            // Devolver los resultados en formato JSON
            echo json_encode($estatus);
    
        } catch (Exception $e) {
            // Manejar la excepción y devolver un mensaje de error en formato JSON
            http_response_code(500);
            echo json_encode(["error" => $e->getMessage()]);
        }
    }

    