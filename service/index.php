<?php 
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

    /** Se llaman dao y modelo seguridad **/
    
    require_once '../dao/SeguridadDAO.php';
    require_once '../modelo/SeguridadModelo.php';
    require_once '../dao/ResidenteDAO.php';
    require_once '../modelo/ResidenteModelo.php';
    require_once '../config/Token.php';

    /** Se declaran variables **/
    $token = $_POST["token"];

    $datos = Token::descifrarToken($token);
    $respuesta = null;
    $jsonAnswer = null;

    if($datos -> tipo == 1){
        $residente = new Residente();
        $residente -> correo = $datos -> correo;
        $respuesta = ResidenteDAO::consultaIndexResidente($residente);
        $jsonAnswer = array('nombre' => $respuesta[0][0], 'apellidos' => $respuesta[0][1], 'vigencia' => $respuesta[0][2], 'direccion' => $respuesta[0][3], 'correo' => $respuesta[0][4], 'telefono' => $respuesta[0][5], 'password' => $respuesta[0][6]);
    }else{
        $seguridad = new Seguridad();
        $seguridad -> correo = $datos -> correo;
        $respuesta = SeguridadDAO::consultaIndexSeguridad($seguridad);
        $jsonAnswer = array('nombre' => $respuesta[0][0], 'apellidos' => $respuesta[0][1], 'vigencia' => "", 'direccion' => "", 'correo' => $respuesta[0][2], 'telefono' => $respuesta[0][3], 'password' => $respuesta[0][4]);
    }

    echo json_encode($jsonAnswer);