<?php 
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

    require_once '../dao/ResidenteDAO.php';
    require_once '../modelo/ResidenteModelo.php';


    /** Se declaran variables **/
    $response = null;
    $code = null;

    $sc = ResidenteDAO::consultaFechaActualQr();
    $response = $sc[0][0];
    $code = "success";

    $jsonAnswer = array("code" => $code, "response" => $response);
    echo json_encode($jsonAnswer);