<?php 
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

    /** Se llaman dao y modelo seguridad **/
    require_once '../dao/SeguridadDAO.php';
    require_once '../modelo/SeguridadModelo.php';
    require_once '../dao/ResidenteDAO.php';
    require_once '../modelo/ResidenteModelo.php';
    require_once '../config/Token.php';

    /** Se declaran variables **/
    $token = $_POST["token"];

    $datos = Token::descifrarToken($token);
    $respuesta = null;
    $jsonAnswer = null;
    $correos = array();
    $telefonos = array();

    /** se obtiene el id del fracccionamiento **/
    $respuesta = ResidenteDAO::consultaFraccionamientoResidente($datos -> id);

    /** se consulta datos de seguridad  **/
    $rs = SeguridadDAO::consultaDatosSeguridad($respuesta[0][0]);
    $lada = "52";

    if(count($rs) > 0 && count($rs) <= 20){
        for($i = 0; $i < count($rs); $i++){
            if($rs[$i][0] != "" && $rs[$i][0] != null){
                array_push($correos,$rs[$i][0]);
            }
            if($rs[$i][1] != "" && $rs[$i][1] != null){
                array_push($telefonos,$lada.$rs[$i][1]);
            }
        }
    }

    if(count($correos) > 0 && count($telefonos)){
        /** Se envia sms **/
        $obtieneTelefonos = implode(",",$telefonos);

        $obtieneEmails = implode(",",$correos);

        /** envio SMS **/
        
        $curl = curl_init();
                                                
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.easysendsms.app/bulksms',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => 'username=alfralfr4ydmv2024&password=KJBT0JxD&to='.$obtieneTelefonos.'&from=ALKI&text=La alerta de pánico fue activada&type=0',
        CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded'
        ),
        ));
                                                    
        $response = curl_exec($curl);
        curl_close($curl);
        echo $response;
    }
