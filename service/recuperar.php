<?php     
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

    /** Se llaman dao y modelo seguridad **/
    require_once '../dao/SeguridadDAO.php';
    require_once '../modelo/SeguridadModelo.php';
    require_once '../dao/ResidenteDAO.php';
    require_once '../modelo/ResidenteModelo.php';
    require_once '../mailer/Enviar.php';
    
    $correo = $_POST["correo"];
    $mensaje = null;
    $status = null;

    if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $correo)){
        /** Se consulta datos a seguridad **/
        $seguridad = new Seguridad();
        $seguridad -> correo = $correo;
        $respuestaSeguridad = SeguridadDAO::consultaSeguridad($seguridad);
        $tipo = 0;
        
        if(isset($respuestaSeguridad[0]) && in_array($correo,$respuestaSeguridad[0])){
            $tipo = 1;
        }

        /** Se consulta datos a residente **/
        $residente = new Residente();
        $residente -> correo = $correo;
        $respuestaResidente = ResidenteDAO::consultaResidente($residente);
        
        if(isset($respuestaResidente[0]) && in_array($correo,$respuestaResidente[0])){
            $tipo = 2;
        }

        /** Valida tipo de usuario **/
        if($tipo == 1){
            $newPass = restaurarSeguridad($respuestaSeguridad,$seguridad);
            $status = "success";
            $mensaje = enviarCorreo($correo,$newPass);
        }

        if($tipo == 2){
            $newPass = restaurarResidente($respuestaResidente,$residente);
            $status = "success";
            $mensaje = enviarCorreo($correo,$newPass);
        }
    }

    function restaurarSeguridad($respuesta,$seguridad){
        /** Modifica seguridad **/
        $nuevaPassword = null;
        $estatus = SeguridadDAO::consultaEstatusSeguridad($seguridad);
        if(isset($respuesta[0]) && !in_array("Baja",$estatus[0])){
            /** Se restaura estatus **/
            $seguridad -> estatus = "Activo";
            SeguridadDAO::editaEstatusSeguridad($seguridad);
            /** Se restaura password **/
            $seguridad -> password = generarCodigo(10);
            SeguridadDAO::editaPasswordSeguridad($seguridad);
            $nuevaPassword = $seguridad -> password;
        }
        return $nuevaPassword;
    }

    function restaurarResidente($respuesta,$residente){
        /** Modifica residente **/
        $nuevaPassword = null;
        $estatus = ResidenteDAO::consultaEstatusResidente($residente);
        if(isset($respuesta[0]) && !in_array("Baja",$estatus[0])){
            /** Se restaura estatus **/
            $residente -> estatus = "Activo";
            ResidenteDAO::editaEstatusResidente($residente);
            /** Se restaura password **/
            $residente -> password = generarCodigo(10);
            ResidenteDAO::editaPasswordResidente($residente);
            $nuevaPassword = $residente -> password;
        }
        return $nuevaPassword;
    }

    function generarCodigo($longitud) {
        $key = '';
        $pattern = ['1','2','3','4','5','6','7','8','9','0','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','!','#','$','%','*'];
        $max = count($pattern)-1;
        for($i=0;$i < $longitud;$i++){
            $key .= $pattern[mt_rand(0,$max)];
        }
        return $key;
    }

    function enviarCorreo($correo,$password){
        $enviado = false;
        $mensaje = "Error al enviar el correo";
        if($correo != null && $password != null){
            $enviado = preparaCorreo($correo, $password);
        }
        /** Se valida envio de correo **/
        if($enviado){
            $mensaje = "El correo fue enviado exitosamente";
        }
        return $mensaje;
    }

    function preparaCorreo($email, $codigo){
        $enviado =false;
        if($email != null && $codigo != null){
            Enviar::enviarPassword($email,$codigo);
            $enviado = true;
        }
        return $enviado;
    }

    $jsonAnswer = array('status' => $status, 'mensaje' => $mensaje);
    echo json_encode($jsonAnswer);