<?php 
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

    /** Se llaman dao y modelo seguridad **/
    require_once '../dao/SeguridadDAO.php';
    require_once '../modelo/SeguridadModelo.php';
    require_once '../dao/ResidenteDAO.php';
    require_once '../modelo/ResidenteModelo.php';
    require_once '../config/Token.php';

    /** Se declaran variables **/
    $correo = $_POST["correo"];
    $password = $_POST["password"];
    $token = null;
    $mensaje = null;

    if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $correo) 
        && 
        preg_match('/^[0-9a-zA-Z!#$%*@]+$/',$password)){

        /** Se consulta datos a seguridad **/
        $seguridad = new Seguridad();
        $seguridad -> correo = $correo;
        $respuestaSeguridad = SeguridadDAO::consultaSeguridad($seguridad);
        $tipo = 0;
        $estatus = SeguridadDAO::consultaEstatusSeguridad($seguridad);
        
        if($respuestaSeguridad && in_array($password,$respuestaSeguridad[0]) && !in_array("Bloqueado",$estatus[0])){
            $id = (int) $respuestaSeguridad[0][array_key_last($respuestaSeguridad[0])];
            $clave = array('correo' => $correo, 'tipo' => 0, 'id' => $id, 'live' => date("Y-m-d H:i:s",mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"))));
            $token = Token::generaToken(json_encode($clave));
            $mensaje = "success";
        }
        if(isset($respuestaSeguridad[0]) && in_array($correo,$respuestaSeguridad[0])){
            $tipo = 1;
        }

        /** Se consulta datos a residente **/
        $residente = new Residente();
        $residente -> correo = $correo;
        $respuestaResidente = ResidenteDAO::consultaResidente($residente);
        $estatus = ResidenteDAO::consultaEstatusResidente($residente);
        
        if($respuestaResidente && in_array($password,$respuestaResidente[0]) && !in_array("Bloqueado",$estatus[0])){
            $id = (int) $respuestaResidente[0][array_key_last($respuestaResidente[0])];
            $clave = array('correo' => $correo, 'tipo' => 1,'id' => $id, 'live' => date("Y-m-d H:i:s",mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"))));
            $token = Token::generaToken(json_encode($clave));
            $mensaje = "success";
        }
        if(isset($respuestaResidente[0]) && in_array($correo,$respuestaResidente[0])){
            $tipo = 2;
        }

        /** Valida tipo de usuario **/
        if($tipo == 1 && $mensaje != "success"){
            $mensaje = modificaSeguridad($respuestaSeguridad,$seguridad);
            if($mensaje != null){
                $token = null;
            }
        }

        if($tipo == 2 && $mensaje != "success"){
            $mensaje = modificaResidente($respuestaResidente,$residente);
            if($mensaje != null){
                $token = null;
            }
        }
    }

    function modificaSeguridad($respuesta,$seguridad){
        /** Modifica seguridad **/
        $mensaje = null;
        if(isset($respuesta[0])){
            $estatus = SeguridadDAO::consultaEstatusSeguridad($seguridad);
            if(in_array("Activo",$estatus[0])){
                $seguridad -> estatus = "Intento-1";
                SeguridadDAO::editaEstatusSeguridad($seguridad);
                $mensaje = "Verifique sus datos, primer intento";
            }
            if(in_array("Intento-1",$estatus[0])){
                $seguridad -> estatus = "Intento-2";
                SeguridadDAO::editaEstatusSeguridad($seguridad);
                $mensaje = "Verifique sus datos, segundo intento";
            }
            if(in_array("Intento-2",$estatus[0])){
                $seguridad -> estatus = "Bloqueado";
                SeguridadDAO::editaEstatusSeguridad($seguridad);
                $mensaje = "Verifique sus datos, usuario bloqueado";
            }
            if(in_array("Bloqueado",$estatus[0])){
                $mensaje = "El usuario esta bloqueado, Póngase en contacto con el administrador";
            }
            if(in_array("Baja",$estatus[0])){
                $mensaje = "El usuario esta dado de baja, Póngase en contacto con el administrador";
            }
        }
        return $mensaje;
    }

    function modificaResidente($respuesta,$residente){
        /** Se modifica residente **/
        $mensaje = null;

        if(isset($respuesta[0])){
            $estatus = ResidenteDAO::consultaEstatusResidente($residente);
            if(in_array("Activo",$estatus[0])){
                $residente -> estatus = "Intento-1";
                ResidenteDAO::editaEstatusResidente($residente);
                $mensaje = "Verifique sus datos, primer intento";
            }
            if(in_array("Intento-1",$estatus[0])){
                $residente -> estatus = "Intento-2";
                ResidenteDAO::editaEstatusResidente($residente);
                $mensaje = "Verifique sus datos, segundo intento";
            }
            if(in_array("Intento-2",$estatus[0])){
                $residente -> estatus = "Bloqueado";
                ResidenteDAO::editaEstatusResidente($residente);
                $mensaje = "Verifique sus datos, usuario bloqueado";
            }
            if(in_array("Bloqueado",$estatus[0])){
                $mensaje = "El usuario esta bloqueado, Póngase en contacto con el administrador";
            }
            if(in_array("Baja",$estatus[0])){
                $mensaje = "El usuario esta dado de baja, Póngase en contacto con el administrador";
            }
        }
        return $mensaje;
    }

    $jsonAnswer = array('token' => $token, 'mensaje' => $mensaje);
    echo json_encode($jsonAnswer);