<?php 
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

    /** Se llaman dao y modelo seguridad **/
    require_once '../dao/SeguridadDAO.php';
    require_once '../modelo/SeguridadModelo.php';
    require_once '../dao/ResidenteDAO.php';
    require_once '../modelo/ResidenteModelo.php';
    require_once '../config/Token.php';

    /** Se declaran variables **/
    $token = $_POST["token"];

    $datos = Token::descifrarToken($token);
    $respuesta = null;

    if($datos -> tipo == 1){
        $residente = new Residente();
        $residente -> correo = $datos -> correo;
        $respuesta = ResidenteDAO::consultaAlertaResidente($residente);
    }else{
        $respuesta = SeguridadDAO::consultaAlertaSeguridad();
    }

    $data = array();
    
    $datos = array('data' => $respuesta);

    if(count($datos["data"]) > 0){
        for($i = 0; $i < count($datos["data"]); $i++){
            $response = ['id' => $datos["data"][$i][0], 'fecha' => $datos["data"][$i][1], 'direccion' => $datos["data"][$i][2], 'estatus' => $datos["data"][$i][3]];
            array_push($data, $response);
        }
    }

    echo json_encode($data);