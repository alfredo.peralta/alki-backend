<?php 
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

    require_once '../dao/ResidenteDAO.php';
    require_once '../modelo/ResidenteModelo.php';
    require_once '../mailer/Enviar.php';

    /** Se declaran variables **/
    $nombre = $_POST["nombre"];
    $cantidad = $_POST["cantidad"];
    $acceso = $_POST["acceso"];
    $id = $_POST["id"];
    $fecha = $_POST["fecha"];
    $code = null;
    $response = null;
    $descripcion = null;
    $personas = "";
    $isResidente = "";
    $direccion = null;

    if(preg_match('/^[0-9]+$/',$id) && preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/',$nombre)){

        if(($fecha != null || $fecha != "") && $acceso == "v"){
        /** consulta direccion **/
        $direccion = ResidenteDAO::consultaDireccionResidente($id);

        /** consultaAccesoQr  **/
        $sc = ResidenteDAO::consultaAccesoQrResidente($id,$fecha);
        
        if($sc != null && count($sc) > 0){
            $code = "info";
            $response = "El Código Qr ya fue escaneado";
        }else{
            if($acceso == "v" && $cantidad == 1){
                $descripcion = "Ha ingresado ".$nombre." y en camino ";
            }
            if($acceso == "s" && $cantidad == 1){
                $descripcion = "Se ha retirado ".$nombre."";
            }
            if(($acceso == "v" || $acceso == "s") && $cantidad > 1){
                $entraron = $cantidad - 1;
                $tipoAcceso = ($acceso == "v") ? "Ha ingresado " : "Se ha retirado ";
                if($entraron == 1){
                    $descripcion = $tipoAcceso.$nombre." y "."1 persona más";
                    $personas = " y "." 1 persona más";
                }else{
                    $descripcion = $tipoAcceso.$nombre." y ".$entraron." personas más";
                    $personas = " y ".$entraron." personas más";
                }
            }
            /** Se inserta acceso **/
            if(count($direccion) > 0){
                $descripcion = $descripcion." a ".$direccion[0][0];
            }
            ResidenteDAO::insertaVisitasResidente($id,$cantidad,$descripcion,$fecha);
            $obtenerCorreo = ResidenteDAO::consultaCorreoResidenteporId($id);
            if(count($obtenerCorreo) > 0){
                Enviar::enviarNotificacion($obtenerCorreo[0][0]);
            }
            $tipoAcceso = ($acceso == "v") ? "ingreso" : "retiro";
            $response = "Se ha dado acceso de ".$tipoAcceso." a ".$isResidente.$nombre. $personas;
            $code = "success";
            }
        }

        if(($fecha != null || $fecha != "") && $acceso == "r"){
                /** valida residente **/
                $existe = ResidenteDAO::consultaFraccionamientoResidente($id);
                if(count($existe) > 0){
                    /** se valida vigencia y estatus de residente **/
                    $ver = ResidenteDAO::consultaEstatusResidentePorId($id);
                    if(count($ver) > 0){
                        if($ver[0][0] != "Suspendido"){
                            $response = $nombre . " <br/> Residente: Activo";
                            $code = "success";
                        }else{
                            $response = $nombre . " <br/> Residente: Moroso";
                            $code = "info";
                        }
                    }else{
                        $response = null;
                        $code = null;
                    }
                }else{
                    $response = null;
                    $code = null;
                }
        }
    }else{
        $response = "Qr no válido.";
        $code = "error";
    }

    if($code != "info"){
        if($direccion != null && count($direccion) > 0){
            $response = $response." a ".$direccion[0][0];
        }
    }

    $jsonAnswer = array("code" => $code, "response" => $response);

    echo json_encode($jsonAnswer);
