<?php 

    require_once '../app/App.php';
    require_once '../modelo/PrivadaModelo.php';

    class PrivadaDAO{

        public static function listarPrivadas(){
            $con = new App();
            $sql = "SELECT * FROM PRIVADA";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }
        
    }