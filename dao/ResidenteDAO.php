<?php 

    require_once '../app/App.php';
    require_once '../modelo/ResidenteModelo.php';

    class ResidenteDAO{

        public static function listarResidente(){
            $con = new App();
            $sql = "SELECT * FROM residente";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaResidente($residente){
            $con = new App();
            $sql = "SELECT correo, cast(aes_decrypt(password,'alki') as char) as password, id_residente as id FROM residente WHERE correo = '".$residente -> correo."' limit 1";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaCorreoResidenteporId($id){
            $con = new App();
            $sql = "SELECT correo FROM residente WHERE id_residente = ".$id."";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaIndexResidente($residente){
            $con = new App();
            $sql = "SELECT residente.nombre, apellidos, fecha_pago as vigencia, CONCAT(fraccionamiento.nombre,', ',privada.nombre,' ',numero) as direccion, correo, telefono, cast(aes_decrypt(password,'alki') as char) as password from residente INNER JOIN privada on residente.id_privada=privada.id_privada INNER JOIN fraccionamiento on privada.id_fraccionamiento=fraccionamiento.id_fraccionamiento where residente.correo = '".$residente -> correo."'";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaAlertaResidente($residente){
            $con = new App();
            $sql = "SELECT id_alerta, alerta.fecha, CONCAT('Dirección: ',privada.nombre, ' ',residente.numero, ', Tel. ', residente.telefono, ' solicitado por ',residente.nombre,' ',residente.apellidos) as direccion, alerta.estatus from alerta inner join residente on alerta.id_residente=residente.id_residente inner join privada on residente.id_privada=privada.id_privada where residente.correo = '".$residente -> correo."' order by fecha desc";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaDireccionResidente($id){
            $con = new App();
            $sql = "SELECT CONCAT(privada.nombre, ' ',residente.numero) as direccion from residente inner join privada on residente.id_privada=privada.id_privada where residente.id_residente = ".$id;
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaVisitasResidente($id){
            $con = new App();
            $sql = "SELECT fecha_ingreso AS fecha, descripcion FROM acceso INNER JOIN residente ON acceso.id_residente=residente.id_residente WHERE residente.id_residente = ".$id." order by fecha_ingreso desc";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaServicioResidente($id){
            $con = new App();
            $sql = "SELECT CASE WHEN now()<=fecha_pago THEN 'Activo' ELSE 'Vencio' END as servicio FROM residente WHERE id_residente = ".$id;
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaAccesoQrResidente($id,$fecha){
            $con = new App();
            $sql = "SELECT fecha_vigencia FROM acceso WHERE id_residente = '".$id."' AND fecha_vigencia = '".$fecha."'";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaFechaActualQr(){
            $con = new App();
            $sql = "SELECT DATE_ADD(NOW(), INTERVAL 3 HOUR) as fecha";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaFechaQrVisita($fecha){
            $con = new App();
            $sql = "SELECT CASE WHEN now()<='".$fecha."' THEN 'Activo' ELSE 'Vencio' END as acceso";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function insertaVisitasResidente($id,$personas,$descripcion,$fecha){
            $con = new App();
            $sql = "INSERT INTO acceso (fecha_ingreso,id_residente,personas,descripcion,fecha_vigencia) VALUES(NOW(),".$id.",".$personas.",'".$descripcion."','".$fecha."')";
            $respuesta = $con -> ejecutaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function insertaAlertaResidente($id){
            $con = new App();
            $sql = "INSERT INTO alerta (id_residente,descripcion,estatus) VALUES(".$id.",'','En Proceso')";
            $respuesta = $con -> ejecutaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaEstatusResidente($residente){
            $con = new App();
            $sql = "SELECT estatus FROM residente WHERE correo = '".$residente -> correo."' limit 1";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaEstatusResidentePorId($id){
            $con = new App();
            $sql = "SELECT estatus FROM residente WHERE id_residente = ".$id;
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaFraccionamientoResidente($id){
            $con = new App();
            $sql = "SELECT fraccionamiento.id_fraccionamiento AS id FROM fraccionamiento INNER JOIN residente ON fraccionamiento.id_fraccionamiento=residente.id_fraccionamiento WHERE residente.id_residente = ".$id."";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function registrarResidente($residente){
            $con = new App();
            $respuesta = false;
            $sql = "INSERT INTO residente(nombre,apellidos,id_privada,numero,fechaAlta,correo,password,telefono,id_fraccionamiento) 
            VALUES('".$residente -> nombre."','".$residente -> apellidos."','".$residente -> id_privada."','".$residente -> numero."','".$residente -> fechaAlta."','".$residente -> correo."', aes_encrypt('".$residente -> password."','alki'),'".$residente -> telefono."','".$residente -> id_fraccionamiento."')";
            $respuesta = $con -> ejecutaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }
        
        public static function editaResidente($residente){
            $con = new App();
            $respuesta = false;
            $sql = "UPDATE residente SET nombre = '".$residente -> nombre."',apellidos = '".$residente -> apellidos."',estatus = '".$residente -> estatus."',fechaAlta = '".$residente -> fechaAlta."',correo = '".$residente -> correo."',password = aes_encrypt('".$residente -> password."','alki'), ".
            " id_fraccionamiento = ".$residente -> id_fraccionamiento.", estatus = '".$residente -> id_fraccionamiento."', telefono = '".$residente -> telefono."' ".
            "WHERE id_residente = ".$residente -> id_residente." ";
            $respuesta = $con -> ejecutaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function editaIndexResidente($residente){
            $con = new App();
            $respuesta = false;
            $sql = "UPDATE residente SET nombre = '".$residente -> nombre."',apellidos = '".$residente -> apellidos."',correo = '".$residente -> correo."',password = aes_encrypt('".$residente -> password."','alki')".
            ", telefono = '".$residente -> telefono."' ".
            "WHERE id_residente = ".$residente -> id_residente." ";
            $respuesta = $con -> ejecutaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function editaEstatusResidente($residente){
            $con = new App();
            $respuesta = false;
            $sql = "UPDATE residente SET estatus = '".$residente -> estatus."' ".
            "WHERE correo = '".$residente -> correo."' ";
            $respuesta = $con -> ejecutaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function editaPasswordResidente($residente){
            $con = new App();
            $respuesta = false;
            $sql = "UPDATE residente SET password = aes_encrypt('".$residente -> password."','alki') ".
            "WHERE correo = '".$residente -> correo."' ";
            $respuesta = $con -> ejecutaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }
        
        public static function eliminaResidente($residente){
            $con = new App();
            $respuesta = false;
            $sql = "DELETE FROM residente WHERE id_residente = ".$residente -> id_residente." ";
            $respuesta = $con -> ejecutaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }


        
    }