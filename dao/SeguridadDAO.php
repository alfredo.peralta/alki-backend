<?php 

    require_once '../app/App.php';
    require_once '../modelo/SeguridadModelo.php';

    class SeguridadDAO{

        public static function listarSeguridad(){
            $con = new App();
            $sql = "SELECT * FROM seguridad";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaSeguridad($seguridad){
            $con = new App();
            $sql = "SELECT correo, cast(aes_decrypt(password,'alki') as char) as password, id_seguridad as id FROM seguridad WHERE correo = '".$seguridad -> correo."' limit 1";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaEstatusSeguridad($seguridad){
            $con = new App();
            $sql = "SELECT estatus FROM seguridad WHERE correo = '".$seguridad -> correo."' limit 1";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaEstatusSeguridadPorId($id){
            $con = new App();
            $sql = "SELECT estatus FROM seguridad WHERE id_seguridad = ".$id;
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function registrarSeguridad($seguridad){
            $con = new App();
            $respuesta = false;
            $sql = "INSERT INTO seguridad(nombre,apellidos,estatus,fechaAlta,correo,password,id_fraccionamiento) 
            VALUES('".$seguridad -> nombre."','".$seguridad -> apellidos."','".$seguridad -> estatus."','".$seguridad -> fechaAlta."','".$seguridad -> correo."',aes_encrypt('".$seguridad -> password."','alki'),'".$seguridad -> id_fraccionamiento."')";
            $respuesta = $con -> ejecutaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }
        
        public static function consultaIndexSeguridad($seguridad){
            $con = new App();
            $sql = "SELECT seguridad.nombre, apellidos, correo, telefono, cast(aes_decrypt(password,'alki') as char) as password from seguridad INNER JOIN fraccionamiento on seguridad.id_fraccionamiento=fraccionamiento.id_fraccionamiento where seguridad.correo = '".$seguridad -> correo."'";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaDatosSeguridad($id){
            $con = new App();
            $sql = "SELECT correo, telefono FROM seguridad inner join fraccionamiento on seguridad.id_fraccionamiento=fraccionamiento.id_fraccionamiento where fraccionamiento.id_fraccionamiento = ".$id." AND seguridad.estatus != 'Bloqueado' AND seguridad.estatus != 'Baja'";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function consultaAlertaSeguridad(){
            $con = new App();
            $sql = "SELECT id_alerta, alerta.fecha, CONCAT('Dirección: ',privada.nombre, ' ',residente.numero, ', Tel. ', residente.telefono, ' solicitado por ',residente.nombre,' ',residente.apellidos) as direccion, alerta.estatus from alerta inner join residente on alerta.id_residente=residente.id_residente inner join privada on residente.id_privada=privada.id_privada order by fecha desc";
            $respuesta = $con -> consultaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function editaEstatusAlertaSeguridad($estatus,$id){
            $con = new App();
            $respuesta = false;
            $sql = "UPDATE alerta SET estatus = '".$estatus."' ".
            "WHERE id_alerta = ".$id." ";
            $respuesta = $con -> ejecutaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function editaSeguridad($seguridad){
            $con = new App();
            $respuesta = false;
            $sql = "UPDATE seguridad SET nombre = '".$seguridad -> nombre."',apellidos = '".$seguridad -> apellidos."',estatus = '".$seguridad -> estatus."',fechaAlta = '".$seguridad -> fechaAlta."',correo = '".$seguridad -> correo."',password = aes_encrypt('".$seguridad -> password."','alki'), ".
            " id_fraccionamiento = ".$seguridad -> id_fraccionamiento." ".
            "WHERE id_seguridad = ".$seguridad -> id_seguridad." ";
            $respuesta = $con -> ejecutaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function editaIndexSeguridad($seguridad){
            $con = new App();
            $respuesta = false;
            $sql = "UPDATE seguridad SET nombre = '".$seguridad -> nombre."',apellidos = '".$seguridad -> apellidos."',correo = '".$seguridad -> correo."',password = aes_encrypt('".$seguridad -> password."','alki')".
            ", telefono = '".$seguridad -> telefono."' ".
            "WHERE id_seguridad = ".$seguridad -> id_seguridad." ";
            $respuesta = $con -> ejecutaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function editaEstatusSeguridad($seguridad){
            $con = new App();
            $respuesta = false;
            $sql = "UPDATE seguridad SET estatus = '".$seguridad -> estatus."' ".
            "WHERE correo = '".$seguridad -> correo."' ";
            $respuesta = $con -> ejecutaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }

        public static function editaPasswordSeguridad($seguridad){
            $con = new App();
            $respuesta = false;
            $sql = "UPDATE seguridad SET password = aes_encrypt('".$seguridad -> password."','alki') ".
            "WHERE correo = '".$seguridad -> correo."' ";
            $respuesta = $con -> ejecutaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }
        
        public static function eliminaSeguridad($seguridad){
            $con = new App();
            $respuesta = false;
            $sql = "DELETE FROM seguridad WHERE id_seguridad = ".$seguridad -> id_seguridad." ";
            $respuesta = $con -> ejecutaMysql($sql);
            $con -> cerrar();
            return $respuesta;
        }
        
    }
