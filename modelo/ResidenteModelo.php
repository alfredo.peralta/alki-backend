<?php 

    class Residente{

        public $id_residente; 
        public $nombre;
        public $apellidos;
        public $id_privada;
        public $numero;
        public $fechaAlta;
        public $correo;
        public $password;
        public $estatus;
        public $fecha_pago;
        public $telefono;
        public $id_fraccionamiento;

        public function __construct(){}

        public function __destruct(){}

    }
